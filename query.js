const fs = require('fs')
const Graph = require('./graph')
const data = require('./data')

const graph = data.graph
const FILM_ARR = data.filmList
const ACTOR_ARR = data.actorList

function listFilms(actorName) {
    console.log(`All films '${actorName}' has been in:`)
    const resultList = FILM_ARR.filter(filmObject => filmObject.name in (graph.vertices[actorName] || {}));
    for(let filmObject of resultList) {
        filmObject.weight = graph.vertices[filmObject.name][actorName]
        filmObject.individualGross = data.formatEarning(filmObject.weight)
    }
    resultList.sort((a,b) => b.weight - a.weight)
    console.table(resultList, ['name', 'releaseYear', 'gross', 'actors', 'individualGross']);
}

function listActors(filmName) {
    console.log(`All actors from film '${filmName}':`)
    const resultList = ACTOR_ARR.filter(actorObject => actorObject.name in (graph.vertices[filmName] || {}));
    for(let actorObject of resultList) {
        actorObject.weight = graph.vertices[actorObject.name][filmName]
        actorObject.individualGross = data.formatEarning(actorObject.weight)
    }
    resultList.sort((a,b) => b.weight - a.weight)
    console.table(resultList, ['name', 'birthyear', 'gross', 'films', 'individualGross']);
}

function topFilms(num) {
    if(num <= 0) {
        num = 1
    }
    if(num > FILM_ARR.length) {
        num = FILM_ARR.length
    }
    FILM_ARR.sort((a,b) => b.boxOffice - a.boxOffice)
    console.log(`Top ${num} films:`)
    console.table(FILM_ARR.slice(0, num), ['name', 'releaseYear', 'gross', 'actors'])
}

function topActors(num) {
    if(num <= 0) {
        num = 1
    }
    if(num > ACTOR_ARR.length) {
        num = ACTOR_ARR.length
    }
    ACTOR_ARR.sort((a,b) => b.earning - a.earning)
    console.log(`Top ${num} actors:`)
    console.table(ACTOR_ARR.slice(0, num), ['name', 'birthyear', 'gross', 'films'])
}

function oldestActors(num) {
    if(num <= 0) {
        num = 1
    }
    if(num > ACTOR_ARR.length) {
        num = ACTOR_ARR.length
    }
    ACTOR_ARR.sort((a,b) => a.birthyear - b.birthyear)
    console.log(`Oldest ${num} actors:`)
    console.table(ACTOR_ARR.slice(0, num), ['name', 'birthyear', 'gross', 'films'])
}

function filmsInYear(year) {
    console.log(`All films release in ${year}:`)
    const resultList = FILM_ARR.filter(a => a.releaseYear == year)
    console.table(resultList, ['name', 'releaseYear', 'gross', 'actors'])
}

function actorsInYear(year) {
    console.log(`All actors born in ${year}:`)
    const resultList = ACTOR_ARR.filter(a => a.birthyear == year)
    console.table(resultList, ['name', 'birthyear', 'gross', 'films'])
}

function test() {
    let filmName = 'Now You See Me'
    let actorName = 'Morgan Freeman'
    let year = 2000
    let num = 10
    oldestActors(num)
    topActors(num)
    topFilms(num)
    listFilms(actorName)
    listActors(filmName)
    actorsInYear(year)
    filmsInYear(year)
}

test()
console.log(`Data is based on ${ACTOR_ARR.length} actors and ${FILM_ARR.length} films`)
