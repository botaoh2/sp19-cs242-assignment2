class Graph {

    constructor() {
        this.vertices = {}
    }

    addVertex(v) {
        if(v in this.vertices) {
            return false
        }
        this.vertices[v] = {}
        return true
    }

    addEdge(v1, v2, w) {
        if(v1 == v2) {
            return false
        }
        if(v1 in this.vertices && v2 in this.vertices) {
            this.vertices[v1][v2] = w
            this.vertices[v2][v1] = w
            return true
        }
        return false
    }

    removeVertex(v) {
        delete this.vertices[v]
        for(let vertex in this.vertices) {
            delete this.vertices[vertex][v]
        }
    }

}

module.exports = Graph
