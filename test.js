const Graph = require('./graph')
const log = require('./log')
const extract = require('./extract')

//basic functionality
function test1() {
    const graph = new Graph()
    const data = {
        abc: {
            haha: 2,
        },
        haha: {
            abc: 2,
        },
        foo: {},
        bar: {},
    }
    graph.addVertex("abc")
    graph.addVertex("haha")
    graph.addVertex("foo")
    graph.addVertex("bar")

    graph.addEdge("abc", "haha", 2)

    log.info(`Expected: ${JSON.stringify(data)}`)
    log.info(`Actual: ${JSON.stringify(graph.vertices)}`)
    if(JSON.stringify(data) != JSON.stringify(graph.vertices)) {
        log.error("Test 1 failed")
    }else {
        log.success("Test 1 passed")
    }
}

//handle error
function test2() {
    const graph = new Graph()
    const data = {
        abc: {},
        haha: {},
        foo: {},
        bar: {},
    }
    graph.addVertex("abc")
    graph.addVertex("haha")
    graph.addVertex("foo")
    graph.addVertex("bar")

    //same node
    graph.addEdge("abc", "abc", 2)
    //non existing node
    graph.addEdge("nonexistnode", "abc", 2)
    //repeat vertex
    graph.addVertex("abc")

    log.info(`Expected: ${JSON.stringify(data)}`)
    log.info(`Actual: ${JSON.stringify(graph.vertices)}`)
    if(JSON.stringify(data) != JSON.stringify(graph.vertices)) {
        log.error("Test 2 failed")
    }else {
        log.success("Test 2 passed")
    }
}

//more graph test
function test3() {
    const graph = new Graph()
    const data = {
        abc: {
            haha: 3,
        },
        haha: {
            abc: 3,
            foo: 6,
        },
        foo: {
            bar: 4,
            haha: 6,
        },
        bar: {
            foo: 4,
        },
    }
    graph.addVertex("abc")
    graph.addVertex("haha")
    graph.addVertex("foo")
    graph.addVertex("bar")

    graph.addEdge("abc", "haha", 2)
    graph.addEdge("abc", "haha", 3)
    graph.addEdge("foo", "bar", 4)
    graph.addEdge("foo", "haha", 6)

    log.info(`Expected: ${JSON.stringify(data)}`)
    log.info(`Actual: ${JSON.stringify(graph.vertices)}`)
    if(JSON.stringify(data) != JSON.stringify(graph.vertices)) {
        log.error("Test 3 failed")
    }else {
        log.success("Test 3 passed")
    }
}

//extract boxOffice
function test4() {
	const boxOfficeStr = '$28.9 million'
	const expected = 28900000
	const actual = extract.boxOfficeFromString(log, boxOfficeStr)
	log.info(`Input: ${boxOfficeStr}`)
	log.info(`Expected: ${expected}`)
	log.info(`Actual: ${actual}`)
	if(actual == expected) {
		log.success("Test 4 passed")
	}else {
		log.error("Test 4 failed.")
	}
}

//extract year
function test5() {
	const yearStr = '1992-04-031992-04-10'
	const expected = 1992
	const actual = extract.yearFromString(log, yearStr)
	log.info(`Input: ${yearStr}`)
	log.info(`Expected: ${expected}`)
	log.info(`Actual: ${actual}`)
	if(actual == expected) {
		log.success("Test 5 passed")
	}else {
		log.error("Test 5 failed.")
	}
}

function testAll() {
	test1()
	test2()
	test3()
	test4()
	test5()
}

testAll()
