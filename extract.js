const cheerio = require('cheerio')

function parseLink(link) {
	if(!link || !link.length) {
		return null
	}
    let href = link.attr('href')
    if(!href.startsWith('/wiki/')) {
        return null
    }
    if(href.includes('TV') || href.includes('#') || href.substr(6).includes('/')) {
        return null
    }
	return {
		name: link.text().trim()
		,href: link.attr('href')
	}

}

var extract = {

	//Ex: '$28.9 million' -> 28900000
    boxOfficeFromString: function(logger, str) {
        try{
            let newStr = ""
            //remove commas
            for(let c of str) {
                if(c != ",") {
                    newStr += c
                }
            }
            let regex = /\d+(\.\d+)?/
            newStr = newStr.match(regex)[0]
            let num = Number(newStr)
			if(num < 1000) {
				if(str.toLowerCase().includes("billion")) {
					num *= 1e9
				}else if(str.toLowerCase().includes("million")) {
					num *= 1e6
				}else if(str.toLowerCase().includes("thousand")) {
					num *= 1e3
				}
			}
            if(!str.includes("$")) {
                if(str.includes("¥")) {
                    num *= 0.15
                }else if(str.includes("£")){
					num *= 1.13
				}else if(str.includes("SEK")){
					num *= 0.11
				}else if(st.includes("₤")){
					num *= 0.19
				}else {
                    logger.warn(`Unknown currency: ${str}`)
                }
            }
			if(num < 1000) {
				logger.warn(`Unexpectedly low box office: ${str} -> ${num}`)
			}else {
				logger.info(`Box office: '${str}' -> ${num}`)
			}
            return num
        }catch(error) {
            throw `Could not parse box office: ${str}`
        }
    },

	//Ex: '1992-04-031992-04-10' -> 1992
    yearFromString: function(logger, str) {
        try {
            let regex = /(20|19)\d\d/
            let newStr = str.match(regex)[0]
            let year = Number(newStr)
            logger.info(`Year: '${str}' -> ${year}`)
            return Number(newStr)
        }catch(error) {
            throw `Could not parse year: ${str}`
        }
    },

    dataFromTable: function(logger, body, fieldName) {
    	logger.info(`Looking for ${fieldName}`)
    	let index = body.indexOf(fieldName)
    	if(index == -1) {
			logger.info(`dataFromTable: Could not find ${fieldName}`)
    		return null
    	}
    	body = body.slice(index, index+500)
    	let regex = /<td>.+?<\/td>/
    	let matchResult = body.match(regex)
    	if(matchResult == null) {
			logger.info(`dataFromTable: Could not match regex`)
    		return null
    	}
    	body = matchResult[0]
		const $ = cheerio.load(body, { decodeEntities: false })
    	body = $.text()
    	logger.info(`Found ${fieldName}: ${body}`)
    	return body
    },

    textFromSelector: function(logger, $, selector) {
		logger.info(`Looking for ${selector}`)
		let domObject = $(selector)
		if(!domObject || domObject.length == 0) {
			logger.info(`Could not find ${selector}`)
			return null
		}
		return domObject.text()
    },

	releaseYearFromFilm: function(logger, $, filmObject) {
		try{
			let releaseYearStr =
				extract.textFromSelector(logger, $, ".bday")
			||	extract.dataFromTable(logger, $.html(), 'Release date');
			if(releaseYearStr == null) {
				throw "Could not find release year"
			}
			let releaseYear = extract.yearFromString(logger, releaseYearStr)
			logger.debug(`Release year: ${releaseYear}`)
			filmObject.releaseYear = releaseYear
		}catch(error) {
			logger.warn(`releaseYearFromFilm error: ${error}`)
			throw "Could not extract releaseYear from film"
		}
	},

	boxOfficeFromFilm: function(logger, $, filmObject) {
		try {
			let boxOfficeStr = extract.dataFromTable(logger, $.html(), 'Box office')
			if(boxOfficeStr == null) {
				throw "Could not find box office"
			}
			let boxOffice = extract.boxOfficeFromString(logger, boxOfficeStr)
			logger.debug(`Box office: ${boxOffice}`)
			filmObject.boxOffice = boxOffice
		}catch(error) {
			logger.warn(`boxOfficeFromFilm error: ${error}`)
			throw 'Could not extract boxOffice from film'
		}
	},

    actorsFromFilm: function(logger, $, filmObject) {
        try {
            logger.info(`Looking for #Cast`)
        	let cast = $("#Cast")

			if(cast.length == 0) {
				throw "Could not find '#Cast'"
			}

    		let currentNode = cast.parent().next()
    		let castTable = null
    		while(currentNode.length != 0) {
    			if(currentNode.find('.mw-headline').length > 0) {
    				break
    			}
    			if(currentNode.is('ul')) {
    				castTable = currentNode
    				break
    			}
    			castTable = currentNode.find('ul')
    			if(castTable && castTable.length > 0) {
    				break
    			}
    			currentNode = currentNode.next()
    		}

			if(castTable == null || castTable.length == 0) {
				throw "Could not find castTable"
			}

			let actorLinks = castTable.first().find('a')
			if(actorLinks.length == 0) {
				throw "Zero links from castTable"
			}
			logger.debug(`Found ${actorLinks.length} casts.`)
			filmObject.actors = []
			for(let index=0 ; index<actorLinks.length ; index++) {
				let link = actorLinks.eq(index)
				let linkData = parseLink(link)
                if(linkData) {
                    filmObject.actors.unshift(linkData)
                }
			}
        }catch(error) {
			logger.warn(`actorsFromFilm error: ${error}`)
            throw "Could not extract actors from film"
        }
    },

	birthyearFromActor: function(logger, $, actorObject) {
		try{
			let birthyearStr =
				extract.textFromSelector(logger, $, ".bday")
			||  extract.dataFromTable(logger, $.html(), 'Born')
			if(birthyearStr == null) {
				throw "Could not find birth year"
			}
			let birthyear = extract.yearFromString(logger, birthyearStr)
			logger.debug(`Birth year: ${birthyear}`)
			actorObject.birthyear = birthyear
		}catch(error) {
			logger.warn(`birthyearFromActor error: ${error}`)
			throw "Could not extract birthyear from actor"
		}
	},

    filmsFromActor: function(logger, $, actorObject) {
        try {
            logger.info(`Looking for #Film or #Filmography`)
            let filmography = $('#Film, #Films, #Film_roles')
            if(filmography.length == 0) {
               filmography = $("#Filmography, #Complete_filmography, #Partial_filmography, #Selected_filmography")
            }
			if(filmography == null || filmography.length == 0) {
				throw "Could not find filmography section"
			}
            //find film table
            logger.info(`Looking for film table`)
            let currentNode = filmography.eq(0).parent().next()
            let filmTable = null
            while(currentNode.length != 0) {
                //break unti next section
                if(currentNode.find('.mw-headline').length > 0) {
                    break
                }
                //find tbody
                filmTable = currentNode.find('tbody')
                if(filmTable && filmTable.length > 0) {
                    break
                }
                //find ul
                if(currentNode.is('ul')) {
                    filmTable = currentNode
                    break
                }
                filmTable = currentNode.find('ul')
                if(filmTable && filmTable.length > 0) {
                    break
                }
                currentNode = currentNode.next()
            }
			if(filmTable == null || filmTable.length == 0) {
				throw "Could not find filmTable"
			}
            let filmLinks = filmTable.find('a')
			if(filmLinks.length == 0) {
				throw "Could not extract films from actor"
			}
			logger.debug(`Found ${filmLinks.length} films.`)
			actorObject.films = []
            for(let index=0 ; index<filmLinks.length ; index++) {
                let link = filmLinks.eq(index)
                let linkData = parseLink(link)
                if(linkData) {
                    actorObject.films.unshift(linkData)
                }
            }
        }catch(error) {
			logger.warn(`filmsFromActor error: ${error}`)
            throw "Could not extract films from actor"
        }

    },


}

module.exports = extract
