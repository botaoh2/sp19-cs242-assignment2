function getTimeStamp() {
    return new Date().toLocaleTimeString('en-US')
}

var level = 1

class Logger {

    constructor(prefix) {
        if(!prefix) {
            prefix = ""
        }
        this.prefix = prefix
    }

    infoLevel() {
        return 1
    }

    debugLevel() {
        return 2
    }

    successLevel() {
        return 3
    }

    warnLevel() {
        return 4
    }

    errorLevel() {
        return 5
    }

    info(s) {
        if(level <= this.infoLevel()) {
            console.log(`[LOG] ${getTimeStamp()} ${this.prefix} - ${s}`)
        }
    }

    warn(s) {
        if(level <= this.warnLevel()) {
            console.log('\x1b[35m%s\x1b[0m', `[WAR] ${getTimeStamp()} ${this.prefix} - ${s}`);
        }
    }

    debug(s) {
        if(level <= this.debugLevel()) {
            console.log(`[BUG] ${getTimeStamp()} ${this.prefix} - ${s}`);
        }
    }

    error(s) {
        if(level <= this.errorLevel()) {
            console.log('\x1b[31m%s\x1b[0m', `[ERR] ${getTimeStamp()} ${this.prefix} - ${s}`);
        }
    }

    success(s) {
        if(level <= this.successLevel()) {
            console.log('\x1b[32m%s\x1b[0m', `[SUC] ${getTimeStamp()} ${this.prefix} - ${s}`);
        }
    }

    setLevel(newLevel) {
        if(newLevel > this.errorLevel()) {
            newLevel = this.errorLevel()
        }
        level = newLevel
    }

    getLogger(prefix) {
        return new Logger(prefix)
    }

}

module.exports = new Logger("")
