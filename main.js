const request = require("tinyreq")
const cheerio = require('cheerio')
const fs = require('fs')
const os = require('os')
const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});
const log = require('./log')
log.setLevel(log.debugLevel())
const Graph = require('./graph')
const extract = require('./extract')

const HOST = "https://en.wikipedia.org"
const INTERVAL = 500

const firstLinkData = {
    name: "Morgan Freeman",
    href: "/wiki/Morgan_Freeman"
}

const targetActorSize = 10000 //250
const targetFilmSize = 10000 //125

var ALL_ACTORS = {}
var ALL_FILMS = {}
var actorQueue = [firstLinkData]
var filmQueue = []
var visited = new Set()

var terminate = false
rl.on('line', (input) => {
    terminate = true
    rl.close()
});

function saveHTML(href, body) {
    href = href.slice(1)
    fs.writeFile(href, body, (err) => {
        if (err) {
            log.error(`Error saving HTML: ${href}`)
            log.error(err)
            return
        }
        log.info(`${href} has been saved!`);
    });
}

function getBodyFromLink(href, callback) {
    let localPath = href.slice(1)
    if (fs.existsSync(localPath)) {
        let body = fs.readFileSync(localPath, 'utf8')
        callback(body, true)
    }else {
        let url = HOST + href
        log.debug(`Requesting url: ${url}`)
        request({
            url: url
        }).then(body => {
            saveHTML(href, body)
            callback(body, false)
        }).catch(err => {
            log.error(`Error while requesting url: ${url}`)
            log.error(err)
            scrapeNextTimeout(INTERVAL)
        });
    }
}

function scrapeFilm(linkData) {
    //extract name href
    const filmName = linkData.name
    const href = linkData.href
    let logger = log.getLogger(`[Film] ${href}`)
    //check if visited
    if(visited.has(href)) {
        logger.info(`Duplicate href`)
        scrapeNextTimeout(0)
        return
    }else if(filmName in ALL_FILMS) {
        logger.info(`Duplicate name`)
        scrapeNextTimeout(0)
        return
    }
    visited.add(href)

    getBodyFromLink(href, (body, isLocal) => {
        parseFilm(logger, linkData, body)
        if(isLocal) {
            scrapeNextTimeout(0)
        }else {
            scrapeNextTimeout(INTERVAL)
        }
    });

}

function scrapeActor(linkData) {
    //extract name href
    const actorName = linkData.name
    const href = linkData.href
    let logger = log.getLogger(`[Actr] ${href}`)
    //check if visited
    if(visited.has(href)) {
        logger.info(`Duplicate href`)
        scrapeNextTimeout(0)
        return
    }else if(actorName in ALL_ACTORS) {
        logger.info(`Duplicate name`)
        scrapeNextTimeout(0)
        return
    }
    visited.add(href)
    getBodyFromLink(href, (body, isLocal) => {
        parseActor(logger, linkData, body)
        if(isLocal) {
            scrapeNextTimeout(0)
        }else {
            scrapeNextTimeout(INTERVAL)
        }
    });

}

function parseFilm(logger, linkData, body) {
    logger.info(`Parsing html`)
    const filmName = linkData.name
    const href = linkData.href
    const $ = cheerio.load(body, { decodeEntities: false })
    try{
        const filmObject = {name: filmName, href: href}
        //extract releaseYear
        extract.releaseYearFromFilm(logger, $, filmObject)
        //extract boxOffice
        extract.boxOfficeFromFilm(logger, $, filmObject)
        //extract actors
        extract.actorsFromFilm(logger, $, filmObject)
        //add links to queue
        for(let linkData of filmObject.actors) {
            actorQueue.push(linkData)
            if(actorQueue.length > 1000) break
        }
        //remove links
        filmObject.actors = filmObject.actors.map(a => a.name)
        //finish
        ALL_FILMS[filmName] = filmObject
        logger.success(`parse success`)
        return true
    }catch(error) {
        logger.warn(`Could not parse film: ${error}`)
        return false
    }
}

function parseActor(logger, linkData, body) {
    logger.info(`Parsing html`)
    const actorName = linkData.name
    const href = linkData.href
    const $ = cheerio.load(body, { decodeEntities: false })
    try {
        const actorObject = {name: actorName, href: href}
        //extract birthyear
        extract.birthyearFromActor(logger, $, actorObject)
        //extract films
        extract.filmsFromActor(logger, $, actorObject)
        //add links to queue
        for(let linkData of actorObject.films) {
            filmQueue.push(linkData)
            if(filmQueue.length > 1000) break
        }
        //remove links
        actorObject.films = actorObject.films.map(a => a.name)
        //finish
        ALL_ACTORS[actorName] = actorObject
        logger.success(`parse success`)
        return true
    }catch(error) {
        logger.warn(`Could not parse actor: ${error}`)
        return false
    }
}

function scrapeNextTimeout(t) {
    setTimeout(scrapeNext, t+Math.floor(t*Math.random()))//t-2*t seconds between requests
}

function scrapeNext() {
    const actorSize = Object.keys(ALL_ACTORS).length
    const filmSize = Object.keys(ALL_FILMS).length
    const freeMemory = os.freemem()
    log.debug(`actorSize: ${actorSize} filmSize: ${filmSize} Visited Links: ${visited.size} ` +
        `Free Memory: ${freeMemory}`)

    if ( (actorSize >= targetActorSize && filmSize >= targetFilmSize) ||
    (actorQueue.length == 0 && filmQueue.length == 0) || 
    freeMemory < 100000 || terminate) {
        finishScrape()
        return
    }
    if(actorSize < filmSize || filmQueue.length == 0) {
        scrapeActor(actorQueue.shift())
    }else {
        scrapeFilm(filmQueue.shift())
    }

}

function removeBadData() {
    //remove actor.films that are not in ALL_FILMS
    for(let actorName in ALL_ACTORS) {
        ALL_ACTORS[actorName].films = ALL_ACTORS[actorName].films.filter(filmName => filmName in ALL_FILMS);
    }
    //remove film.actor that are not in ALL_ACTORS
    for(let filmName in ALL_FILMS) {
        ALL_FILMS[filmName].actors = ALL_FILMS[filmName].actors.filter(actorName => actorName in ALL_ACTORS);
    }
    //double link
    for(let actorName in ALL_ACTORS) {
        ALL_ACTORS[actorName].films = 
            ALL_ACTORS[actorName].films.filter(filmName => ALL_FILMS[filmName].actors.includes(actorName))
    }
    for(let filmName in ALL_FILMS) {
        ALL_FILMS[filmName].actors = 
            ALL_FILMS[filmName].actors.filter(actorName => ALL_ACTORS[actorName].films.includes(filmName))
    }
}

function calculateEarning() {
    const CREW_SHARE = 20
    const getShare = function (actorName) {
        if(actorName in ALL_ACTORS == false) {
            log.error(`${actorName} is not in list`)
            return 0
        }
        return ALL_ACTORS[actorName].films.length + 5
    };
    const graph = new Graph()
    //generate vertices
    for(let actorName in ALL_ACTORS) {
        graph.addVertex(actorName)
    }
    for(let filmName in ALL_FILMS) {
        graph.addVertex(filmName)
    }
    //generate edges
    for(let filmName in ALL_FILMS) {
        const filmObject = ALL_FILMS[filmName]
        let boxOffice = filmObject.boxOffice
        //sum of all shares of actors plus CREW_SHARE
        let totalShare = filmObject.actors.reduce((total, actorName) => total + getShare(actorName), CREW_SHARE)
        //add edges
        for(let actorName of filmObject.actors) {
            let weight = boxOffice / totalShare * getShare(actorName)
            graph.addEdge(filmName, actorName, weight)
        }
    }
    //calculate earning of actors
    for(let actorName in ALL_ACTORS) {
        const actorObject = ALL_ACTORS[actorName]
        actorObject.earning = 0
        for(let filmName in graph.vertices[actorName]) {
            actorObject.earning += graph.vertices[actorName][filmName]
        }
    }
    return graph
}

function saveData(fileName, data) {
    fs.writeFile(`out/${fileName}`, JSON.stringify(data, null, '\t'), (err) => {
        if(err){
            log.error(`Write ${fileName} failed!`)
        }else {
            log.success(`${fileName} file saved!`)
        }
    })
}

function formatEarning(num) {
    if(num >= 1e9) {
        num /= 1e9
        return `$${num.toFixed(1)} billion`
    }
    if(num >= 1e6) {
        num /= 1e6
        return `$${num.toFixed(1)} million`
    }
    return `$${num.toFixed(0)}`
}

function finishScrape() {
    log.success("Scrape Finish")
    log.debug(`Total actors: ${Object.keys(ALL_ACTORS).length}`)
    log.debug(`Total films: ${Object.keys(ALL_FILMS).length}`)
    
    actorQueue = []
    filmQueue = []
    visited = null

    removeBadData()
    const graph = calculateEarning()
    let actorList = []
    let filmList = []
    saveData('graph.json', graph)

    //convert data to list
    for(let actorName in ALL_ACTORS) {
        let actorObject = ALL_ACTORS[actorName]
        actorList.push({
            name: actorObject.name,
            birthyear: actorObject.birthyear,
            earning: actorObject.earning,
            films: actorObject.films.length,
            gross: formatEarning(actorObject.earning),
            href: actorObject.href,
        })
    }
    saveData('actorList.json', actorList)
    for(let filmName in ALL_FILMS) {
        let filmObject = ALL_FILMS[filmName]
        filmList.push({
            name: filmObject.name,
            releaseYear: filmObject.releaseYear,
            boxOffice: filmObject.boxOffice,
            actors: filmObject.actors.length,
            gross: formatEarning(filmObject.boxOffice),
            href: filmObject.href,
        })
    }
    saveData('filmList.json', filmList)

    saveData('ALL_ACTORS.json', ALL_ACTORS)
    saveData('ALL_FILMS.json', ALL_FILMS)


}

scrapeNext()
